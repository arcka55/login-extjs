#!/bin/sh

sudo su

cd ~

echo "Update software repository"
yum update -y

timezone="Asia/Makassar"
read -p "Masukkan Timezone [Tekan Enter: $timezone]: "
if [[ $REPLY != "" ]]; then
    timezone=$REPLY
fi

echo "Set Timezone"
timedatectl set-timezone $timezone

echo "Install tools"
yum -y install yum-utils nano wget unzip

echo "Install httpd"
installed_httpd=`rpm -qa httpd`
if [ "$installed_httpd" = "" ]; then
    yum -y install httpd
    systemctl enable httpd.service
fi 

echo "Install php7+"
installed_epel=`rpm -qa epel-release`
if [ "$installed_epel" = "" ]; then
    yum -y install epel-release
fi
installed_remi=`rpm -qa remi-release`
if [ "$installed_remi" = "" ]; then
    yum -y install http://rpms.remirepo.net/enterprise/remi-release-7.rpm
    yum-config-manager --enable remi-php74
fi
installed_php=`rpm -qa php`
if [ "$installed_php" = "" ]; then
    yum -y install php-common php php-cli php-pdo php-mysqlnd php-opcache php-fpm php-soap php-intl php-pear php-bcmath php-gd php-json php-pecl-mcrypt php-mbstring
fi

echo "Install JDK 8"
if [ ! -f jdk-8u261-linux-x64.rpm ]; then
    #wget -q https://oraclemirror.np.gy/jdk8/jdk-8u261-linux-x64.rpm
    wget -q -O jdk-8u271-linux-x64.rpm https://javadl.oracle.com/webapps/download/AutoDL?BundleId=243711_61ae65e088624f5aaa0b1d2d801acb16
fi
installed_jdk=`rpm -qa jdk1.8`
if [ "$installed_jdk" = "" ]; then
    yum -y localinstall jdk-8u271-linux-x64.rpm
fi

cd /var/www/html

echo "Copy Configuration Files"
yes | cp install/config/selinux /etc/sysconfig/
sed -i -e 's/\r$//' /etc/sysconfig/selinux

yes | cp install/config/sudoers /etc/
sed -i -e 's/\r$//' /etc/sudoers

yes | cp install/config/00-base.conf /etc/httpd/conf.modules.d/
yes | cp install/config/httpd.conf /etc/httpd/conf/
yes | cp install/config/php.conf /etc/httpd/conf.d/
yes | cp install/config/www.conf /etc/php-fpm.d/

echo "Reload Daemon Service"
systemctl daemon-reload

echo "Enabled Service"
systemctl enable httpd
systemctl enable php-fpm

echo "Start Service"
systemctl start httpd
systemctl start php-fpm

setenforce 0

cd ~

echo "install composer"
if [ ! -f /usr/bin/composer ]; then
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    php composer-setup.php --install-dir=/usr/bin --filename=composer
    php -r "unlink('composer-setup.php');"
fi

echo "install sdk ExtJs"
if [ ! -d ext-6.2.0 ]; then
    wget -q http://cdn.sencha.com/ext/gpl/ext-6.2.0-gpl.zip
    unzip -q ext-6.2.0-gpl.zip
    rm -rf ext-6.2.0-gpl.zip
fi

echo  "Get sencha cmd"
installed_sencha_cmd=`sudo ls -l /root/bin/Sencha/Cmd/6.2.1.29/ | grep sencha.jar`
if [ "$installed_sencha_cmd" = "" ]; then
    if [ ! -f SenchaCmd-6.2.1.29-linux-amd64.sh ]; then
        wget -q http://cdn.sencha.com/cmd/6.2.1.29/no-jre/SenchaCmd-6.2.1.29-linux-amd64.sh.zip
        unzip -q SenchaCmd-6.2.1.29-linux-amd64.sh.zip
        rm -rf SenchaCmd-6.2.1.29-linux-amd64.sh.zip
    fi  
    sudo ./SenchaCmd-6.2.1.29-linux-amd64.sh -q
fi
