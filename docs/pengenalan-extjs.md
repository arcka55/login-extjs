# PENGENALAN ExtJS

`ExtJS` adalah Javascript Framework yang mendukung arsitektur aplikasi MVC dan MVVM. Dimana ExtJS memiliki banyak komponen yang dibutuhkan untuk pembuatan aplikasi web desktop maupun web mobile (Hybrid)

## ARSITEKTUR ExtJS

### Apa itu MVC?

Dalam arsitektur MVC, sebagian besar kelas adalah Model, Tampilan (View) dan  Pengendali (Controller). Pengguna berinteraksi dengan Views, yang menampilkan data yang disimpan dalam Models. Interaksi tersebut dipantau oleh Pengendali, yang kemudian merespons interaksi dengan memperbarui Tampilan dan Model, sebagaimana diperlukan.

Tampilan dan Model pada umumnya tidak mengetahui satu sama lain karena Pengendali memiliki tanggung jawab untuk mengarahkan pembaruan. Secara umum, Pengendali akan mengandung sebagian besar logika aplikasi dalam aplikasi MVC. Pandangan idealnya memiliki sedikit (jika ada) logika bisnis. Model terutama merupakan antarmuka ke data dan berisi logika bisnis untuk mengelola perubahan pada data tersebut.

Tujuan MVC adalah untuk secara jelas mendefinisikan tanggung jawab untuk setiap kelas dalam aplikasi. Karena setiap kelas memiliki tanggung jawab yang jelas, mereka secara implisit dipisahkan dari lingkungan yang lebih besar. Ini membuat aplikasi lebih mudah untuk diuji dan dipelihara, dan kodenya lebih dapat digunakan kembali.

### Apa itu MVVM?

Perbedaan utama antara MVC dan MVVM adalah bahwa MVVM menampilkan abstraksi dari View yang disebut ViewModel. ViewModel mengoordinasikan perubahan antara data Model dan presentasi View tentang data itu menggunakan teknik yang disebut `data binding`.

Hasilnya adalah bahwa Model dan kerangka kerja melakukan sebanyak mungkin pekerjaan, meminimalkan atau menghilangkan logika aplikasi yang secara langsung memanipulasi Tampilan.

![Gambar](img/gambar-03.01.png)

Sumber: <https://woxapp.com/our-blog/the-use-of-the-modelviewviewmodel-pattern-on-android/>

## PENGECEKAN APLIKASI

Sebelum pembuatan project terlebih dahulu silahkan lakukan pengecekan sbb:

- Jika belum menjalankan `vagrant up` silahkan dijalankan perintah tersebut
- Akses ssh vagrant `vagrant ssh`
- Masuk ke root `sudo su`
- Cek folder SDK ExtJS `ls -l ~`

    ![Gambar](img/gambar-03.02.png)

- Cek webserver dan php

```bash
systemctl status httpd php-fpm
```

![Gambar](img/gambar-03.03.png)

- Cek akses website <http://192.168.137.10>

    ![Gambar](img/gambar-03.04.png)

- Cek apakah sencha berhasil terinstal `sencha`

    ![Gambar](img/gambar-03.05.png)

Jika semua sudah berjalan dengan baik maka lakukan langkah - langkah berikut:

## PEMBUATAN PROJECT (WORKSPACE & APPLICATION)

**Workspace** adalah area kerja pada aplikasi maupun paket

### PEMBUATAN WORKSPACE

```bash
cd ~/ext-6.2.0

sencha generate workspace /var/www/html/latihan/application
```

atau

```bash
cd /var/www/html/latihan

sencha -sdk ~/ext-6.2.0 generate workspace application
```

Seperti tampak pada gambar dibawah ini

![Gambar](img/gambar-03.06.png)

### PEMBUATAN APPLICATION

Nama aplikasi yang akan dibuat yaitu `SIMRS`

```bash
# Aktif pada direktori latihan/application
cd /var/www/html/latihan/application

# Lakukan generate aplikasi SIMRS
sencha -sdk ./ext generate app SIMRS ./SIMRS
```

Seperti tampak pada gambar dibawah ini

![Gambar](img/gambar-03.07.png)

Setelah itu, silahkan akses lewat browser dengan alamat <http://192.168.137.10/latihan/application/SIMRS>

## PEMBUATAN PACKAGE

**Package** adalah modul / librari yang terpisah dari application sehingga paket ini dapat digunakan pada aplikasi yang membutuhkan.

Nama paket yang akan dibuat yaitu `pasien`

```bash
# Aktif pada direktori application
cd application

# Lakukan generata paket pasien
sencha generate package -f ext -ty code pasien
```

![Gambar](img/gambar-03.08.png)

## BUILD PACKAGE & APPLICATION

### BUILD PACKAGE

Sebelum melakukan build package, terlebih dahulu kita melakukan konfigurasi paket yang akan di build

- Aktif ke direktori package pasien `cd packages/local/pasien`
- Buat direktori `classic` dan `classic/src`

```bash
mkdir -p classic classic/src
```

- Edit file `package.json`
- Lakukan perubahan isi `"creator"`, `"summary"`, `"detailedDescription"` dan `"version"`
- Lakukan perubahan `"output": "${package.dir}/build",` menjadi `"output": "${workspace.dir}/packages/${build.id}",`
- Tambahkan script dibawah ini sesudah `"request": [],`

```json
"builds": {
    "classic": {
        "toolkit": "classic",
        "theme": "theme-classic"
    },
    "modern": {
        "toolkit": "modern",
        "theme": "theme-neptune"
    }
}
```

- Jika tidak menginginkan build css di package maka lakukan edit file `.sencha\package\sencha.cfg` tambahkan `skip.style=1` sebelum `package.cmd.version`
- Lakukan build package

```bash
# Build classic dan modern
sencha package build

# Build classic
sencha package build classic

# Build modern
sencha package build modern
```

### BUILD APPLICATION

- Aktif ke direktori application SIMRS `cd /var/www/html/latihan/application/SIMRS`
- Edit file `app.json`
- Tambahkan package pasien dari

```json
"js": [
    {
        "path": "app.js",
        "bundle": true
    }
],
```

menjadi

```json
"js": [
    {
        "path": "app.js",
        "bundle": true
    },
    {
        "path": "${workspace.dir}/packages/${toolkit.name}/pasien.js",
        "bundle": true,
        "includeInBundle": true
    }
],
```

- Lakukan build application

```bash
# Build classic dan modern
sencha app build

# Build classic
sencha app build classic

# Build modern
sencha app build modern
```

## STUDI KASUS

- Buatlah **Workspace** dengan tujuan folder `/var/www/html/latihan/myweb/application`
- Buatlah **Application** dengan nama **SDM**
- Buatlah **Package** dengan nama **pegawai**
- Build **Package**
- Build **Application**
