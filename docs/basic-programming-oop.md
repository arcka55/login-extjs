# BASIC PROGRAMMING OOP (OBJECT ORIENTED PROGRAMMING)

**Object Oriented Programming** biasa disingkat **OOP** adalah Suatu paradigma pemrograman berdasarkan konsep object yang didalamnya terdapat data (field/attribute) dan fungsi (method).

## CLASS, INSTANCE & OBJECT

`Class` adalah abstraksi atau kumpulan objek – objek, dimana class mendefinisikan attribute dan fungsi untuk digunakan pada saat objek di buat (instance)

`Instance` adalah pembuatan/pembentukan dari class sehingga menghasilkan objek

`Object` merupakan benda nyata yang dibuat berdasarkan rancangan yang di definisikan di dalam Class

![Gambar](img/gambar-04.01.png)

### Contoh Deklarasi Kelas

Lakukan perintah dibawah ini:

```bash
# Aktif pada package pasien folder examples
cd /var/www/html/latihan/application/packages/local/pasien/examples

# Buat direktory oop
mkdir -p oop

# Aktif pada folder oop
cd oop

# Buat file index.php, Mobil.js dan Mobil.php
echo '' > index.php > Mobil.js > Mobil.php
```

Pada file `Mobil.js` ketik perintah dibawah ini:

```javascript
// Contoh deklarasi Class di ExtJS
Ext.define("Mobil", {});
```

Pada file `Mobil.php` ketik perintah dibawah ini:

```php
<?php
// Contoh deklarasi Class di PHP
class Mobil {}
```

### Contoh Deklarasi Instance & Object

Pada file `index.php` ketik perintah dibawah ini:

```php
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Pelatihan OOP</title>

    <script type="text/javascript" src="../../../../../ext/build/ext-all.js"></script>
    <link rel="stylesheet" type="text/css" href="../../../../../build/production/SIMRS/classic/resources/SIMRS-all.css" />

    <script type="text/javascript" src="Mobil.js"></script>

    <!-- Contoh penggunaan di ExtJS -->
    <script>
        Ext.onReady(function() {
            /**
             * honda dan toyota adalah object
             * new Mobil() & Ext.create("Mobil") adalah instance
             */
            var honda = new Mobil(),
                toyota = Ext.create("Mobil");

            console.log("Honda instance dari class " + Ext.getClassName(honda) + ": " + (honda instanceof Mobil ? "Ya" : "Tdk"));
            console.log("Toyota instance dari class " + Ext.getClassName(toyota) + ": " + (toyota instanceof Mobil ? "Ya" : "Tdk"));
        });
    </script>

    <!-- Contoh Penggunaan di PHP -->
    <?php
        require_once("Mobil.php");

        $honda = new Mobil();
        $toyota = new Mobil();
        echo "Honda instance dari class ".get_class($honda).": ".($honda instanceof Mobil ? "Ya" : "Tdk")."<br/>";
        echo "Toyota instance dari class ".get_class($toyota).": ".($toyota instanceof Mobil ? "Ya" : "Tdk")."<br/>";
    ?>
</head>
<body>
</body>
</html>
```

Untuk melihat hasilnya silahkan klik [disini](http://192.168.137.10/latihan/application/packages/local/pasien/examples/oop/). Hasilnya akan sama dengan gambar dibawah ini.

![Gambar](img/gambar-04.02.png)

## CONSTRUCTOR, METHOD, ATTRIBUTE/FIELD & STATIC

`Constructor` adalah fungsi khusus yang dipanggil ketika class di instance dan mendeklarasikan nilai awal suatu object.

`Method` adalah fungsi yang menggunakan kata kerja di dalam suatu object untuk menjalankan aktifitasnya baik memiliki parameter maupun tidak.

`Attribute/Field` adalah tempat penampungan nilai sementara, baik nilai bertipe number, string maupun object atau biasa disebut variable maupun property suatu object.

`Static` adalah attribute atau fungsi yang di definisikan di dalam class dipanggil tanpa menciptakan object (membuat class)

Pada file `Mobil.js` ketik perintah dibawah ini:

### Contoh Deklarasi Constructor

```javascript
Ext.define("Mobil", {
    contructor: function() {}
});
```

```php
<?php
class Mobil {
    function __construct() {}
}
```

### Contoh Deklarasi Method

```javascript
Ext.define("Mobil", {
    contructor: function() {
    },

    /**
     * @method bukaPintu
     */
    bukaPintu: function() {
    }
});
```

```php
<?php
class Mobil {
    function __construct() {
    }

    /**
     * @method bukaPintu
     */
    function bukaPintu() {
    }
}
```

### Contoh Deklarasi Attribute/Field

```javascript
Ext.define("Mobil", {
    /**
     * @field pintuTerbuka
     */
    pintuTerbuka: false,

    contructor: function() {
        this.pintuTerbuka = false;
    },

    bukaPintu: function() {
        this.pintuTerbuka = true
    },

    tutupPintu: function() {
        this.pintuTerbuka = false
    },

    isPintuTerbuka: function() {
        return this.pintuTerbuka;
    }
});
```

```php
<?php
class Mobil {
    /**
     * @field pintuTerbuka
     */
    private $pintuTerbuka = false;

    function __construct() {
        $this->pintuTerbuka = false;
    }

    function bukaPintu() {
        $this->pintuTerbuka = true;
    }

    function tutupPintu() {
        $this->pintuTerbuka = false;
    }

    function isPintuTerbuka() {
        return $this->pintuTerbuka;
    }
}
```

### Contoh Deklarasi Static

```javascript
Ext.define("Mobil", {
    /**
     * @static field dan method
     */
    statics: {
        bunyiAlarm: false,
        isBunyiAlarm: function() {
            return this.bunyiAlarm;
        }
    },

    pintuTerbuka: false,

    contructor: function() {
        this.pintuTerbuka = false;
    },

    bukaPintu: function() {
        this.pintuTerbuka = true
    },

    tutupPintu: function() {
        this.pintuTerbuka = false
    },

    isPintuTerbuka: function() {
        return this.pintuTerbuka;
    }
});
```

```php
<?php
class Mobil {
    /**
     * @static field
     */
    static $bunyiAlarm = false;

    private $pintuTerbuka = false;

    function __construct() {
        $this->pintuTerbuka = false;
    }

    function bukaPintu() {
        $this->pintuTerbuka = true;
    }

    function tutupPintu() {
        $this->pintuTerbuka = false;
    }

    function isPintuTerbuka() {
        return $this->pintuTerbuka;
    }

    /**
     * @static method
     */
    static function isBunyiAlarm() {
        return self::$bunyiAlarm;
    }
}
```

### Contoh Penggunaan

Pada file `index.php` ubah script pada tag `<script>` untuk code `ExtJS` dan `<?php` untuk code `php`

```php
    <!-- Contoh penggunaan di ExtJS -->
    <script>
        Ext.onReady(function() {
            var honda = Ext.create("Mobil");
            // Pemanggilan fungsi bukaPintu
            honda.bukaPintu();
            console.log("Status Pintu Honda: " + (honda.isPintuTerbuka() ? "Terbuka" : "Tertutup"));

            // Memberikan nilai padai field static
            Mobil.bunyiAlarm = true;
            // Pemanggilan fungsi static
            console.log("Bunyi Alarm: " + (Mobil.isBunyiAlarm() ? "Ya" : "Tdk"));
        });
    </script>

    <!-- Contoh Penggunaan di PHP -->
    <?php
        require_once("Mobil.php");

        $honda = new Mobil();
        $honda->bukaPintu();
        echo "Status Pintu Honda: ".($honda->isPintuTerbuka() ? "Terbuka" : "Tertutup")."<br/>";

        // Memberikan nilai padai field static
        Mobil::$bunyiAlarm = true;
        // Pemanggilan fungsi static
        echo "Bunyi Alarm: ".(Mobil::isBunyiAlarm() ? "Ya" : "Tdk")."<br/>";
    ?>
```

### Contoh Output

![Gambar](img/gambar-04.03.png)

## INHERITANCE

`Inheritance` adalah class turunan (child) dari induk class (parent) dimana attribute dan fungsi yang terdapat pada parent class dimiliki juga oleh child class.

### Contoh Penggunaan Inheritance

Buat file `Bus.js` dan `Bus.php` dimana class `Bus` tersebut merupakan turunan dari class `Mobil`

```javascript
Ext.define("Bus", {
    extend: "Mobil"
});
```

```php
<?php
class Bus extends Mobil {
}
```

Pada file `index.php` ubah script pada tag `<script>` untuk code `ExtJS` dan `<?php` untuk code `php`

```php
    <script type="text/javascript" src="Mobil.js"></script>
    <script type="text/javascript" src="Bus.js"></script>

    <!-- Contoh penggunaan di ExtJS -->
    <script>
        Ext.onReady(function() {
            var honda = Ext.create("Mobil");
            honda.bukaPintu();
            console.log("Status Pintu Honda: " + (honda.isPintuTerbuka() ? "Terbuka" : "Tertutup"));

            Mobil.bunyiAlarm = true;
            console.log("Bunyi Alarm: " + (Mobil.isBunyiAlarm() ? "Ya" : "Tdk"));
            
            var busPariwisata = Ext.create("Bus");
            // fungsi isPintuTerbuka dapat diakses oleh class Bus tanpa di deklarasikan
            console.log("Status Pintu Bus: " + (busPariwisata.isPintuTerbuka() ? "Terbuka" : "Tertutup"));
            console.log("Bunyi Alarm: " + (Bus.bunyiAlarm ? "Ya" : "Tdk"));
        });
    </script>

    <!-- Contoh Penggunaan di PHP -->
    <?php
        require_once("Mobil.php");
        require_once("Bus.php");

        $honda = new Mobil();
        $honda->bukaPintu();
        echo "Status Pintu Honda: ".($honda->isPintuTerbuka() ? "Terbuka" : "Tertutup")."<br/>";

        Mobil::$bunyiAlarm = true;
        echo "Bunyi Alarm: ".(Mobil::isBunyiAlarm() ? "Ya" : "Tdk")."<br/>";

        $busPariwisata = new Bus();
        // fungsi isPintuTerbuka dapat diakses oleh class Bus tanpa di deklarasikan
        echo "Status Pintu Bus: ".($busPariwisata->isPintuTerbuka() ? "Terbuka" : "Tertutup")."<br/>";
        echo "Bunyi Alarm: ".(Bus::isBunyiAlarm() ? "Ya" : "Tdk")."<br/>";
    ?>
```

### Contoh Output Inheritance

![Gambar](img/gambar-04.04.png)

## ENCAPSULATION

`Encapsulation` adalah information hiding (penyembunyian informasi), dimana attribute tdk dapat diakses langsung oleh object yg lain dan hanya dapat di akses melalui method.

### Contoh Penggunaan Encapsulation

```javascript
Ext.define("Bus", {
    extend: "Mobil",

    config: {
        // field ini tidak dapat diakses secara langsung tetapi harus melalui fungsi
        jumlahPenumpang: 25
    },

    constructor: function() {
        // Memanggil kembali contructor parent
        this.callParent();
        // Melakukan inisiasi field config
        this.initConfig(this.config);
    }
});
```

```php
<?php
class Bus extends Mobil {
    // field ini tidak dapat diakses secara langsung tetapi harus melalui fungsi
    private $jumlahPenumpang = 25;

    function setJumlahPenumpang($val) {
        $this->jumlahPenumpang = $val;
    }

    function getJumlahPenumpang() {
        return $this->jumlahPenumpang;
    }
}
```

Pada file `index.php` ubah script pada tag `<script>` untuk code `ExtJS` dan `<?php` untuk code `php`

```php
    <script type="text/javascript" src="Mobil.js"></script>
    <script type="text/javascript" src="Bus.js"></script>

    <!-- Contoh penggunaan di ExtJS -->
    <script>
        Ext.onReady(function() {
            var bus = Ext.create("Bus");
            console.log("Jumlah Penumpang: " + bus.getJumlahPenumpang());
            bus.setJumlahPenumpang(10);
            console.log("Jumlah Penumpang: " + bus.getJumlahPenumpang());
            // Jika diakses seperti dibawah ini maka akan menghasilkan undefined (tidak terdefinisikan)
            console.log("Jumlah Penumpang: " + bus.jumlahPenumpang);
        });
    </script>

    <!-- Contoh Penggunaan di PHP -->
    <?php
        require_once("Mobil.php");
        require_once("Bus.php");

        $bus = new Bus();
        echo "Jumlah Penumpang: ".$bus->getJumlahPenumpang()."<br/>";
        $bus->setJumlahPenumpang(10);
        echo "Jumlah Penumpang: ".$bus->getJumlahPenumpang()."<br/>";
        // Jika diakses seperti dibawah ini maka akan menghasilkan kesalahan
        echo "Jumlah Penumpang: ".($bus->jumlahPenumpang)."<br/>";
    ?>
```

### Contoh Output Encapsulation

![Gambar](img/gambar-04.05.png)
Gambar 02.05

atau

![Gambar](img/gambar-04.06.png)
Gambar 02.06

> Ubah display_error menjadi On di php config `nano /etc/php.ini` maka outputnya akan sama seperti gambar 02.06 diatas.

## POLIMORFISME

`Polimorfisme` adalah aktifitas suatu object dimana memiliki fungsi yg sama tetapi hasilnya berbeda.

### Contoh Penggunaan Polimorfisme

Buat file `MPV.js` dan `MPV.php` dimana class `MPV` tersebut merupakan turunan dari class `Mobil`

```javascript
Ext.define("MPV", {
    extend: "Mobil",

    config: {
        jumlahPenumpang: 7
    },

    constructor: function() {
        this.callParent();
        this.initConfig(this.config);
    }
});
```

```php
<?php
class MPV extends Mobil {
    private $jumlahPenumpang = 7;

    function setJumlahPenumpang($val) {
        $this->jumlahPenumpang = $val;
    }

    function getJumlahPenumpang() {
        return $this->jumlahPenumpang;
    }
}
```

Pada file `index.php` ubah script pada tag `<script>` untuk code `ExtJS` dan `<?php` untuk code `php`

```php
    <script type="text/javascript" src="Mobil.js"></script>
    <script type="text/javascript" src="Bus.js"></script>
    <script type="text/javascript" src="MPV.js"></script>

    <!-- Contoh penggunaan di ExtJS -->
    <script>
        Ext.onReady(function() {
            var bus = Ext.create("Bus"),
                mpv = Ext.create("MPV");

            /**
             * Ada 2 object yang dibuat yaitu bus & mpv dimana object tersebut akan
             * memanggil fungsi yang sama tetapi hasilnya berbeda
             */
            console.log("Jumlah Penumpang Bus: " + bus.getJumlahPenumpang());
            console.log("Jumlah Penumpang MPV: " + mpv.getJumlahPenumpang());
        });
    </script>

    <!-- Contoh Penggunaan di PHP -->
    <?php
        require_once("Mobil.php");
        require_once("Bus.php");
        require_once("MPV.php");

        $bus = new Bus();
        $mpv = new MPV();
        echo "Jumlah Penumpang Bus: ".$bus->getJumlahPenumpang()."<br/>";
        echo "Jumlah Penumpang Bus: ".$mpv->getJumlahPenumpang()."<br/>";
    ?>
```

### Contoh Output Polimorfisme

![Gambar](img/gambar-04.07.png)

## STUDI KASUS

Buatlah class Binatang dimana memiliki child class Kucing dan Anjing, kucing akan mengeluarkan suara meong sedangkan anjing mengeluarkan suara gukguk, beserta penggunaannya.

Pembuatan ini harus memiliki konsep **OOP** dibawah ini:

1. Class, Attribute/Field, Method
1. Inheritance
1. Polimorfisme
1. Encapsulation

> Coding harus sesuai gambar soal dan hasil sesuai dengan gambar hasil soal

![Gambar](img/gambar-04.10.png)

```plantuml
@startuml
class Binatang {
    -string nama
    +string getNama()
    +setNama(string val)
    +suara()
}

class Kucing {
    +suara()
}

class Anjing {
    +suara()
}

Binatang <|-- Kucing
Binatang <|-- Anjing
@enduml
```

Output yang dihasilkan

![Gambar](img/gambar-04.09.png)
