# PENGENALAN KOMPONEN ExtJS

## MODEL DAN STORE

`Model` atau biasa di sebut `Entity` adalah representasi/mewakili beberapa object/tabel yang di kelola oleh aplikasi dimana Model terdiri dari beberapa field

```plantuml
@startuml pasien
class Pasien {
    NORM: int
    NAMA: string
    TEMPAT_LAHIR: string
    TANGGAL_LAHIR: date
    JENIS_KELAMIN: int
    ALAMAT: string
}
@enduml
```

Buat file `Model.js` pada folder `latihan/application/packages/local/pasien/src` dan masukkan kode dibawah ini:

```javascript
Ext.define("pasien.Model", {
    extend: "Ext.data.Model",
    fields: [{
        name: "NORM", 
        type: "int"
    }, {
        name: "NAMA", 
        type: "string"
    }, {
        name: "TEMPAT_LAHIR", 
        type: "string"
    }, {
        name: "TANGGAL_LAHIR", 
        type: "date",
        dateFormat: "Y-m-d"
    }, {
        name: "JENIS_KELAMIN", 
        type: "int",
        defaultValue: 1
    }, {
        name: "ALAMAT", 
        type: "string"
    }],

    idProperty: "NORM"
});
```

`Store` adalah sekumpulan object model (record), dimana memiliki fungsi seperti array

Buat file `Store.js` pada folder `latihan/application/packages/local/pasien/src` dan masukkan kode dibawah ini:

```javascript
Ext.define("pasien.Store", {
    extend: "Ext.data.Store",
    model: "pasien.Model",
    alias: "store.pasien-store"
});
```

**Contoh penggunaan:**

Buat file `index.html` pada package `pasien` folder `examples` dan tambahkan kode dibawah ini

```html
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Pengenalan Komponen Ext JS</title>

    <script type="text/javascript" src="../../../../ext/build/ext-all.js"></script>
    <link rel="stylesheet" type="text/css" href="../../../../build/production/SIMRS/classic/resources/SIMRS-all.css" />
</head>
<body>
</body>
</html>
```

Tambahkan kode dibawah ini pada tag `head` file `index.html`

```html
<script type="text/javascript" src="../src/Model.js"></script>
<script type="text/javascript" src="../src/Store.js"></script>

<script>
    Ext.onReady(function() {
        var ahmad = Ext.create("pasien.Model", {
                NORM: 1,
                NAMA: "Wahidin",
                TEMPAT_LAHIR: "Makassar",
                TANGGAL_LAHIR: Ext.Date.parse("1981-01-01", "Y-m-d"),
                JENIS_KELAMIN: 1,
                ALAMAT: "Jl. Perintis Kemerdekaan"
            }),
            store = Ext.create("pasien.Store");

        store.add(ahmad);
        console.log("Jumlah Data Pasien: " + store.getCount());
        store.add({
            NORM: 2,
            NAMA: "Fatmawati",
            TEMPAT_LAHIR: "Jakarta",
            TANGGAL_LAHIR: Ext.Date.parse("1985-03-31", "Y-m-d"),
            JENIS_KELAMIN: 2,
            ALAMAT: "Jl. RS Fatmawati"
        });
        console.log("Jumlah Data Pasien: " + store.getCount());
        console.log("Ambil data dari store: ");
        store.each(function(row) {
            console.log("NORM: " + row.get("NORM") + "\nNAMA: " + row.get("NAMA"));
        });
    });
</script>
```

![Gambar](img/gambar-06.01.png)

Gambar 06.01 - Contoh hasil kode diatas

## LAYOUT

Ada beberapa layout yang sering digunakan:

1. Fit
1. VBox (Vertical Box)
1. Hbox (Horizontal Box)
1. Border
1. Card

### FIT

`Sub Container / Panel` yang berada di dalam `Root Panel` dengan `layout fit` maka `lebar` dan `tinggi` `sub panel` tersebut akan mengikuti `root panel`.

**Contoh penggunaan:**

- Buat file `fit.html` pada package `pasien` folder `examples` dan tambahkan kode seperti pada pembuatan file `index.html` diatas.
- Ubah judulnya menjadi `Pengenalan Komponen Ext JS - Layout Fit`
- Tambahkan kode dibawah ini pada tag `head` file `fit.html`

```html
<script>
    Ext.onReady(function() {
        Ext.create("Ext.panel.Panel", {
            title: "Fit Layout",
            width: 600,
            height: 200,
            layout: "fit",
            items: {
                title: "Inner Panel",
                html: "This is the inner panel content",
                bodyPadding: 20,
                boder: false
            },
            renderTo: Ext.getBody()
        });
    });
</script>
```

![Gambar](img/gambar-06.02.png)

Gambar 06.02 - Layout Fit

### VBOX

`Sub Panel` yang berada pada `Root Panel` akan bersusun secara vertikal

**Contoh penggunaan:**

- Buat file `vbox.html` pada package `pasien` folder `examples` dan tambahkan kode seperti pada pembuatan file `index.html` diatas.
- Ubah judulnya menjadi `Pengenalan Komponen Ext JS - Layout VBox`
- Tambahkan kode dibawah ini pada tag `body` file `vbox.html`

```html
<script>
    Ext.create("Ext.panel.Panel", {
        title: "VBox Layout",
        width: 600,
        height: 350,
        layout: {
            type: "vbox",
            align: "stretch"
        },
        items: [{
            title: "Panel 1",
            html: "Inner Panel One",
            flex: 2
        }, {
            title: "Panel 2",
            html: "Inner Panel Two",
            flex: 1
        }, {
            title: "Panel 3",
            html: "Inner Panel Three",
            flex: 1
        }],
        renderTo: document.body
    });
</script>
```

![Gambar](img/gambar-06.03.png)

Gambar 06.03 - Layout VBox

### HBOX

`Sub Panel` yang berada pada `Root Panel` akan bersusun secara horizontal

**Contoh penggunaan:**

- Buat file `hbox.html` pada package `pasien` folder `examples` dan tambahkan kode seperti pada pembuatan file `index.html` diatas.
- Ubah judulnya menjadi `Pengenalan Komponen Ext JS - Layout HBox`
- Tambahkan kode dibawah ini pada tag `body` file `hbox.html`

```html
<script>
    Ext.create("Ext.panel.Panel", {
        title: "VBox Layout",
        width: 600,
        height: 350,
        layout: {
            type: "hbox",
            align: "stretch"
        },
        items: [{
            title: "Panel 1",
            html: "Inner Panel One",
            flex: 2
        }, {
            title: "Panel 2",
            html: "Inner Panel Two",
            flex: 1
        }, {
            title: "Panel 3",
            html: "Inner Panel Three",
            flex: 1
        }],
        renderTo: document.body
    });
</script>
```

![Gambar](img/gambar-06.04.png)

Gambar 06.04 - Layout VBox

### BORDER

Pada layout border memiliki beberapa region, jika `Sub Panel` diberikan region `center` maka `sub panel` tersebut akan diletakkan ditengah.

Region:

1. North (Atas)
1. East (Kanan)
1. South (Bawah)
1. West (Kiri)
1. Center (Tengah)

**Contoh penggunaan:**

- Buat file `border.html` pada package `pasien` folder `examples` dan tambahkan kode seperti pada pembuatan file `index.html` diatas.
- Ubah judulnya menjadi `Pengenalan Komponen Ext JS - Layout Border`
- Tambahkan kode dibawah ini pada tag `body` file `border.html`

```html
<script>
    Ext.create("Ext.panel.Panel", {
        title: "Border Layout",
        width: 700,
        height: 400,
        layout: "border",
        items: [{
            xtype: "panel",
            title: "North Region (Atas)",
            region: "north", // position for region
            height: 75,
            split: true // enable resizing
        }, {
            // xtype: 'panel' implied by default
            title: "East Region (Kanan)",
            region: "east",
            width: 200,
            margin: "0 0 2 0",
            collapsible: true // make collapsible
        }, {
            title: "South Region (Bawah)",
            region: "south",
            height: 75
        }, {
            title: "West Region (Kiri)",
            region: "west",
            width: 200,
            margin: "0 0 2 0"
        }, {
            title: "Center Region (Tengah)",
            region: "center", // center region is required, no width & height specified
            margin: "0 2 2 2"
        }],
        renderTo: document.body
    });
</script>
```

![Gambar](img/gambar-06.05.png)

Gambar 06.05 - Layout Border

### CARD

`Sub Panel` yang berada pada layout `card` akan bertumpuk, `sub panel` yang pertama di tambahkan akan berada pada tumpukan pertama dst.

**Contoh penggunaan:**

- Buat file `card.html` pada package `pasien` folder `examples` dan tambahkan kode seperti pada pembuatan file `index.html` diatas.
- Ubah judulnya menjadi `Pengenalan Komponen Ext JS - Layout Card`
- Tambahkan kode dibawah ini pada tag `body` file `card.html`

```html
<script>
    var card = Ext.create("Ext.panel.Panel", {
        title: "Card Layout",
        width: 600,
        height: 350,
        layout: "card",
        items: [{
            title: "Card 1",
            html: "Card One"
        }, {
            title: "Card 2",
            html: "Card Two"
        }, {
            title: "Card 3",
            html: "Card Three"
        }],
        renderTo: Ext.getBody()
    });

    // aktif card 2
    card.getLayout().setActiveItem(1);
</script>
```

![Gambar](img/gambar-06.06.png)

Gambar 06.06 - Layout Card

## FORM

`Form` adalah Container untuk menampung component input seperti textfield, combobox, datefield dll.

**Contoh penggunaan:**

- Buat file `Form.js` pada package `pasien` folder `classic/src` dan tambahkan kode dibawah ini

```javascript
Ext.define("pasien.Form", {
    extend: "Ext.form.Panel",
    xtype: "pasien-form", // or alias: "widget.pasien-form"
    title: "Pasien Form",
    bodyPadding: 15,
    layout: {
        type: "vbox",
        align: "stretch"
    },
    items: [{
        xtype: "numberfield",
        name: "NORM",
        fieldLabel: "No. RM",
        hideTrigger: true,
        allowBlank: false,
        maxLength: 8,
        enforceMaxLength: true
    }, {
        xtype: "textfield",
        name: "NAMA",
        fieldLabel: "Nama",
        allowBlank: false
    }, {
        xtype: "textfield",
        name: "TEMPAT_LAHIR",
        fieldLabel: "Tempat Lahir",
        allowBlank: false
    }, {
        xtype: "datefield",
        name: "TANGGAL_LAHIR",
        fieldLabel: "Tgl. Lahir",
        allowBlank: false,
        format: "d/m/Y"
    }, {
        xtype: "radiogroup",
        fieldLabel: "Jenis Kelamin", 
        columns: 2,
        items: [{
            boxLabel: "Laki-laki",
            name: "JENIS_KELAMIN",
            inputValue: 1,
            checked: true
        }, {
            boxLabel: "Perempuan",
            name: "JENIS_KELAMIN",
            inputValue: 2
        }]
    }, {
        xtype: "textfield",
        name: "ALAMAT",
        fieldLabel: "Alamat"
    }],
    buttons: [{
        text: "Reset",
        handler: function() {
            this.up("form").getForm().reset();
        }
    }, {
        text: "Simpan",
        handler: function() {
            Ext.toast("Data berhasil disimpan"); // Ext.Msg.alert("info", "Data berhasil disimpan");
        }
    }]
});
```

- Buat file `form.html` pada package `pasien` folder `examples` dan tambahkan kode seperti pada pembuatan file `index.html` diatas.
- Ubah judulnya menjadi `Pengenalan Komponen Ext JS - Form`
- Tambahkan kode dibawah ini pada tag `head` file `form.html`

```html
<script type="text/javascript" src="../classic/src/Form.js"></script>
```

- Tambahkan kode dibawah ini pada tag `body` file `form.html`

```html
<script>
    Ext.onReady(function() {
        Ext.create("pasien.Form", {
            width: 700,
            height: 400,
            renderTo: Ext.getBody()
        });
    });
</script>
```

![Gambar](img/gambar-06.07.png)

Gambar 06.07 - Pasien Form

## GRID

`Grid` adalah Component yang dapat menampilkan data yang terdiri dari baris dan kolom

**Contoh penggunaan:**

- Buat file `Grid.js` pada package `pasien` folder `classic/src` dan tambahkan kode dibawah ini

```javascript
Ext.define("pasien.Grid", {
    extend: "Ext.grid.Panel",
    xtype: "pasien-grid",
    title: "Daftar Pasien",
    store: {
        type: "pasien-store"
    },
    columns: [{
        xtype: "rownumberer",
        text: "No",
        align: "left",
        width: 50
    }, {
        text: "No. RM",
        dataIndex: "NORM",
        align: "center",
        width: 80
    }, {
        text: "Nama",
        dataIndex: "NAMA",
        flex: 1
    }]
});
```

- Buat file `grid.html` pada package `pasien` folder `examples` dan tambahkan kode seperti pada pembuatan file `index.html` diatas.
- Ubah judulnya menjadi `Pengenalan Komponen Ext JS - Grid`
- Tambahkan kode dibawah ini pada tag `head` file `form.html`

```html
<script type="text/javascript" src="../src/Model.js"></script>
<script type="text/javascript" src="../src/Store.js"></script>
<script type="text/javascript" src="../classic/src/Grid.js"></script>
```

- Tambahkan kode dibawah ini pada tag `body` file `grid.html`

```html
<script>
    Ext.onReady(function() {
        var grid = Ext.create("pasien.Grid", {
            width: 700,
            renderTo: Ext.getBody()
        });

        grid.getStore().loadData([
            { NORM: 12345678, NAMA: "Wahidin", TEMPAT_LAHIR: "Makassar" },
            { NORM: 87654321, NAMA: "Fatmawati", TEMPAT_LAHIR: "Jakarta" }
        ]);
    });
</script>
```

![Gambar](img/gambar-06.08.png)

Gambar 06.08 - Daftar Pasien

## CONTROLLER

Tugas `Controller` adalah untuk menangani interaksi pengguna (event handler)

**Contoh penggunaan:**

- Buat file `FormController.js` pada package `pasien` folder `classic/src`

```javascript
Ext.define("pasien.FormController", {
    extend: "Ext.app.ViewController",
    alias: "controller.pasien-form",

    onReset: function() {
        this.getView().getForm().reset();
    },

    onSimpan: function() {
        Ext.toast("Data berhasil disimpan");
    }
});
```

- Edit file `Form.js` pada package `pasien` folder `classic/src`
- Tambahkan kode ini sesudah `xtype` pada `Form.js`

```javascript
controller: "pasien-form",
```

- `handler: function() {` pada tombol `reset` diubah menjadi `handler: "onReset"`
- Tambahkan `formBind: true` pada tombol `simpan`
- `handler: function() {` pada tombol `simpan` diubah menjadi `handler: "onSimpan"`
- Edit file `form.html` tambahkan `<script type="text/javascript" src="../classic/src/FormController.js"></script>` pada tag `head`

![Gambar](img/gambar-06.09.png)

Gambar 06.09 - Form Controller

## VIEW MODEL

`View Model` adalah Menghubungkan atau mengikat (binding) antara view dan model

- Edit file `Form.js`
- Tambahkan kode ini sesudah `xtype` atau `controller`

```javascript
viewModel: {
    data: {
        record: Ext.create("pasien.Model", {
            NORM: 12345678,
            NAMA: "Wahidin",
            TEMPAT_LAHIR: "Makassar",
            TANGGAL_LAHIR: Ext.Date.parse("1980-01-01", "Y-m-d"),
            JENIS_KELAMIN: 1,
            ALAMAT: "Jl. Perintis Kemerdekaan"
        })
    }
},
```

- Pada items dengan field `name: "NORM"`, di akhir field tambahkan `bind: { value: "{record.NORM}" }`
- Pada items dengan field `name: "NAMA"`, di akhir field tambahkan `bind: "{record.NAMA}"`
- Pada items dengan field `name: "TEMPAT_LAHIR"`, di akhir field tambahkan `bind: "{record.TEMPAT_LAHIR}"`
- Pada items dengan field `name: "TANGGAL_LAHIR"`, di akhir field tambahkan `bind: "{record.TANGGAL_LAHIR}"`
- Pada items dengan field `fieldLabel: "Jenis Kelamin"`, di akhir field tambahkan `simpleValue: true, bind: "{record.JENIS_KELAMIN}"`
- Pada items dengan field `name: "ALAMAT"`, di akhir field tambahkan `bind: { value: "{record.ALAMAT}" }`
- Edit file `FormController.js`
- Ubah kode pada event `onSimpan` seperti kode dibawah ini

```javascript
var vm = this.getViewModel(),
    record = vm.get("record");
Ext.toast(
    "Data berhasil disimpan<br>" +
    "No. RM: <b>" + record.get("NORM") + "</b><br>" +
    "Nama: <b>" + record.get("NAMA") + "</b><br>" +
    "Tempat Lahir: <b>" + record.get("TEMPAT_LAHIR") + "</b><br>" +
    "Tanggal Lahir: <b>" + Ext.Date.format(record.get("TANGGAL_LAHIR"), "d/m/Y") + "</b><br>" +
    "Jenis Kelamin: <b>" + (record.get("JENIS_KELAMIN") == 1 ? "Laki-laki" : "Perempuan") + "</b><br>" +
    "Alamat: <b>" + record.get("ALAMAT") + "</b><br><br>"
);
```

- Pada file `form.html`
- Tambahkan kode `<script type="text/javascript" src="../src/Model.js"></script>` ini pada tag `head`  sebelum kode `<script type="text/javascript" src="../classic/src/Form.js"></script>`

![Gambar](img/gambar-06.10.png)

Gambar 06.10 - View Model

> **Referensi:** <https://docs.sencha.com/extjs/6.2.0/classic/Ext.html>

## MENGGUNAKAN API

- Edit file `Model.js`
- Tambahkan kode dibawah ini

```javascript
proxy: {
    type: "rest,
    url: "/api/pendaftaran/pasien"
}
```

- Edit file `grid.html`
- Ubah kode dibawah ini

```html
grid.getStore().loadData([
    { NORM: 12345678, NAMA: "Wahidin", TEMPAT_LAHIR: "Makassar" },
    { NORM: 87654321, NAMA: "Fatmawati", TEMPAT_LAHIR: "Jakarta" }
]);
```

menjadi

```html
grid.getStore().load();
```

## MEMASUKKAN PACKAGE PASIEN KE SIMRS

- Pastikan package `pasien` sudah terdaftar pada file `app.json` folder `SIMRS`
- Edit file `Main.js` pada folder `SIMRS\classic\src\view\main`
- Tambahkan kode ini pada `items` diakhir item `Settings`

```javascript
}, {
    title: 'Daftar Pasien',
    iconCls: 'fa-users',
    items: [{
        xtype: 'pasien-grid',
        autoLoad: true
    }]
```

- Lakukan build package pasien

```bash
cd /var/www/html/latihan/application/packages/local/pasien
sencha package build classic
```

- Lakukan build app SIMRS

```bash
cd /var/www/html/latihan/application/SIMRS
sencha app build classic
```

- Jalankan aplikasi <http://192.168.137.10/apps/SIMRS>
