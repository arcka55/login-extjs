# PENGGUNAAN GIT & VAGRANT

Pastikan aplikas git dan vagrant telah di install.

## PERINTAH - PERINTAH GIT YANG SERING DIGUNAKAN

[Git](https://git-scm.com/docs/git) adalah sistem kontrol revisi terdistribusi yang cepat, skalabel, dengan kumpulan perintah yang sangat kaya yang menyediakan operasi tingkat tinggi dan akses penuh ke internal

Manfaat lain git:

- Sebagai media penyimpanan (repositori)
- Mengetahui perubahan kode sumber
- Memudahkan dalam kolaborasi pengembangan

Dan masih banyak lagi manfaat lainnya

## CONFIG

Untuk melakukan pengaturan awal pada git, seperti `username` dan `email`

```git
git config --global user.name "User Name Anda"
git config --global user.email "Email Anda"
```

### INIT

Untuk memulai atau menginisiasi pembuatan repositori di sisi lokal.

> Gunakan perintah ini jika tidak menggunakan perintah `clone`

```git
git init --initial-branch=main
```

### CLONE

Untuk mengambil atau melakukan kloning repositori dari remote git tanpa melakukan perintah `init` diatas

```git
git clone http://git.simpel.web.id/simpel-development/pelatihan-dasar-teknis-simpel.git
```

### ADD

Perintah `add` digunakan untuk menyiapkan / menambah file pada tahap `staging` sebelum dilakukan penyimpanan pada repositori local.

```git
# Menambah semua file yang baru atau diubah
git add .

# Menambah file tertentu
git add README.md
```

### COMMIT

Perintah `commit` digunakan untuk melakukan penyimpanan ke repositori lokal, sesudah perintah `add` dilakukan

```git
git commit -m "Pesan / Judul Anda yang identik dengan yang anda lakukan"
```

### PUSH

Perintah `push` digunakan untuk menyimpan / mengirim penambahan, perubahan dan penghapusan yang dilakukan dilokal ke remote branch main repositori atau branch tertentu

```git
git push origin main
```

### PULL

Perintah `pull` digunakan untuk mengambil penambahan, perubahan dan penghapusan yang ada di remote dimasukkan ke dalam lokal repositori

```git
git pull origin main
```

> Lakukan perintah ini sebelum melakukan kegiatan seperti penambahan, perubahan dan penghapusan dll

### BRANCH

Perintah `branch` digunakan untuk menampilkan daftar branch, membuat branch baru, menghapus branch dll

#### Menampilkan daftar branch

```git
git branch
```

#### Membuat branch baru dengan nama pendaftaran-pasien

```git
git branch pendaftaran-pasien
```

#### Menghapus branch pendaftaran-pasien

```git
git branch -d pendaftaran-pasien
```

### CHECKOUT

Perintah `checkout` digunakan untuk aktif pada branch tersebut

```git
git checkout pendaftaran-pasien
```

> Untuk mengetahui apakah aktif pada branch tersebut lihat perintah `Menampilkan daftar branch`

### MERGE

Perintah `merge` digunakan untuk melakukan penggabungan antar branch

#### Branch main di merge ke branch pendaftaran-pasien

```git
# Aktif pada branch pendaftaran-pasien
git checkout pendaftaran-pasien

# Merge dengan branch main
git merge main
```

#### Branch pendaftaran-pasien di merge ke branch main

```git
# Aktif pada branch main
git checkout main

# Merge dengan branch pendaftaran-pasien
git merge pendaftaran-pasien -m "Pendaftaran Pasien"
```

### STATUS

Perintah `status` digunakan untuk menampilkan status branch apakah ada penambahan, perubahan, penghapusan dll

```git
git status
```

### LOG

Perintah `log` digunakan untuk menampilkan histori penyimpanan pada repositori local

```git
git log --oneline
```

### Non Aktifkan Prompt Login Git

```git
git config --system --unset credential.helper
git config credential.helper store
```

## PERINTAH - PERINTAH PADA VAGRANT

Sebelum menggunakan `Vagrant` pastikan sudah melakukan instalasi `VirtualBox`

### Install Plugin VB Guest

Sebelum menggunakan vagrant maka harus di install `plugins vb guest` pertama kali

```vagrant
vagrant plugin install vagrant-vbguest
```

> Perintah ini hanya dilakukan sekali

### Install Box

Lakukan perintah ini jika ingin menggunakan box yang diinginkan seperti box `CentOS7`

Silahkan klik untuk melihat daftar [Vagrant Box](https://app.vagrantup.com/boxes/search)

```vagrant
vagrant box add centos/7
```

> Perintah ini hanya dilakukan sekali

### UP

Perintah `up` digunakan untuk memulai menjalankan vagrant baik setelah maupun sesudah proses inisiasi pertama kali (menjalankan Virtual Mesin)

```vagrant
vagrant up
```

Jalankan project `pelatihan-dasar-teknis-simpel` menggunakan vagrant

```bash
# buka cmd atau menggunakan command Visual Studio Code
# Aktif pada direktori pelatihan-dasar-teknis-simpel misalkan direktori tersebut berada di drive D: folder git
cd D:\git\pelatihan-dasar-teknis-simpel

# Selanjutnya jalankan perintah ini
vagrant up

# Jika ada error lakukan yang ada di CATATAN
```

### HALT

Perintah `halt` digunakan untuk mematikan `Virtual Mesin`

```vagrant
vagrant halt
```

### SSH

Perintah `ssh` digunakan untuk akses ssh `Virtual Mesin`

```vagrant
vagrant ssh

# Untuk masuk ke user root
sudo su
```

> Password default `vagrant` jika diminta untuk memasukkan password

### DESTROY

Perintah `destroy` digunakan untuk menghapus vagrant (Virtual Mesin) yang telah digunakan sebelumnya

```vagrant
vagrant destroy
```

**CATATAN:**
> Jika terjadi error pada saat vagrant up pertama kali maka gunakan perintah ini `vagrant ssh -c 'sudo yum -y update kernel'` setelah itu lakukan `vagrant reload --provision`
