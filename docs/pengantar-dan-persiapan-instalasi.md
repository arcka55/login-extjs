# PENGANTAR & PERSIAPAN INSTALASI

## APA ITU SIMpel

- Pada sisi pengguna
`SIMpel` adalah `Sistem Informasi Pelayanan` / `Information Service` yang melayani pengguna dalam melaksanakan tugas tugasnya.
- Developer
`SIMpel` adalah Sistem yang `sederhana` & `mudah`, dimana `team` dituntut untuk selalu berusaha berfikir `simpel` dalam proses `pembuatan` maupun `pengembangan sistem`.

## [ARSITEKTUR](https://docs.simrsgosv2.simpel.web.id/docs/pengantar/arsitektur/)

## [METODE PENGEMBANGAN](hhttps://docs.simrsgosv2.simpel.web.id/docs/pengantar/metode/)

## [PERALATAN PENGEMBANGAN](https://docs.simrsgosv2.simpel.web.id/docs/pengantar/peralatan/)

## STRUKTUR APLIKASI

### FRONTEND

- webapps
  - application (sencha workspace)
    - packages
      - com
      - data
      - aplikasi
      - master
      - pasien
      - pendaftaran
      - kunjungan
      - layanan
      - pembayaran
      - informasi
      - rekammedis
      - penjualan
      - inventory
      - laporan
      - plugins
      - dst
    - SIMpel (Main Aplikasi)

### BACKEND

#### API

- webservice (apigility zend framework)
  - config
  - module
    - DBService
    - Aplikasi
    - General
    - Pendaftaran
    - Layanan
    - Pembayaran
    - Penjualan
    - Inventory
    - MedicalRecord
    - Cetakan
    - Informasi
    - Pembatalan
    - ReportService
    - Plugins
    - BPJService
    - INACBGService
    - dst
  - photos
  - reports

#### DATABASE

- aplikasi
- master
- generator
- pendaftaran
- layanan
- pembayaran
- penjualan
- inventory
- laporan
- cetakan
- pembatalan
- informasi
- medicalrecord
- bpjs
- inacbg
- dst

## ATURAN PENGCODEAN

### FRONTEND DAN BACKEND API

- Nama Class

    Nama Class setiap huruf pertama pada kata huruf besar. Contoh: `P`endaftaran`P`asien

- Nama Method

  Setiap kata diawal huruf kecil dan kata selanjutnya huruf pertama huruf besar. Contoh: konsul`P`asien

- Nama Attributes / Fields (Variables)

  Penulisan sama dengan nama method. Contoh: daftar`P`asien / _daftar`P`asien

- Nama Package

  Penulisan nama package semua huruf kecil dan dapat dipisahkan menggunakan `-`. Contoh: pendaftaranpasien / pendaftaran-pasien

### BACKEND DB

- Nama Tabel

  Penulisan nama tabel semua huruf kecil. Contoh: pasien

- Nama Fields

  Penulisan nama field semua huruf besar dan dapat dipisahkan menggunakan `_` (underscore). Contoh: TEMPAT_LAHIR

- Nama Procedure / Function

  Setiap kata diawal huruf kecil dan kata selanjutnya huruf pertama huruf besar. Contoh: pendaftaran`P`asien

- Nama Trigger

  Contoh: onPasienBeforeInsert / pasien_before_insert

## KEBUTUHAN APLIKASI

- [VirtualBox & Extension Pack](https://www.virtualbox.org/wiki/Downloads)
- [Vagrant](https://www.vagrantup.com/downloads)
- [Git](https://git-scm.com/download/win)

## Peralatan Lainnya

- [Visual Studi Code](https://code.visualstudio.com/download)
- [JDK 8](https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html)
- [TIBCO Jaspersoft Studio 6.11.0](https://community.jaspersoft.com/project/jaspersoft-studio/releases)
- [HeidiSQL](https://www.heidisql.com/download.php)

> Silahkan di download link diatas kemudian di install
