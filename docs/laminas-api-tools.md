# LAMINAS API TOOLS

Adalah alat untuk pembuatan **API** (_Application Programming Interface_), yang dirancang untuk menyederhanakan pembuatan dan pemeliharaan API yang berguna, mudah digunakan, dan terstruktur dengan baik.

Didalam **API** tersebut terdiri dari 1 atau banyak **Service** baik service **Rest** maupun service **RPC**.

Sebelumnya nama **Laminas API Tools** yaitu **Apigility** yang menggunakan kerangka Zend Framework, setelah di alihkan ke **Linux Foundation** maka nama **Zend Framework** menjadi **Laminas** dan **Apigility** menjadi **Laminas API Tools**.

Lihat website [**Laminas**](https://getlaminas.org/)

## Instalasi

```bash
# Aktif pada direktory latihan
cd /var/www/html/latihan

# create project laminas api tools
composer create-project laminas-api-tools/api-tools-skeleton webservice
```

Jika ada pertanyaan `Do you trust "laminas/laminas-component-installer" to execute code and wish to enable it now? (writes "allow-plugins" to composer.json) [y,n,d,?]` maka tekan tombol `y`

Jika ada pertanyaan `Please select which config file you wish to inject 'Laminas\I18n'` tekan tombol `Enter`

Jika ada pertanyaan `Remember this option for other packages of the same type? (Y/n)` maka tekan tombol `y`

## Update Laminas API Tools

```bash
# Aktif pada direktory webservice
cd /var/www/html/latihan/webservice

# create project laminas api tools
composer update
```

## Reinstall Laminas API Tools

```bash
# Aktif pada direktory webservice
cd /var/www/html/latihan/webservice

rm composer.lock && rm -rf vendor
composer install
```

## Mode Laminas API Tools

```bash
# Aktif pada direktory webservice
cd /var/www/html/latihan/webservice

# Cek status mode
composer development-status

# Development Mode
composer development-enable

# Operasional Mode
composer development-disable
```

## Menjalankan Laminas API Tools

- Untuk menjalankan **Laminas API Tools** tanpa menggunakan **Apache** yaitu:

```bash
composer serve
```

> Akses **Laminas API Tools** ke <http://192.168.137.10:8080>

- Menjalakan **Laminas API Tools** menggunakan **Apache**

```bash
# Copy file alias.conf pada folder install/config ke httpd config
cp /var/www/html/install/config/alias.conf /etc/httpd/conf.d

# Restart service httpd
systemctl restart httpd
```

> Akses **Laminas API Tools** ke <http://192.168.137.10/api>

Maka hasilnya akan tampak seperti gambar dibawah ini:

![Gambar](img/gambar-05.01.png)

Gambar 05.01

## Membuat API

- Pada gambar 05.01 klik tombol `New API`
- Masukkan nama **API** misalkan `Pendaftaran`
- Tekan tombol `Create` untuk membuat **API**

Seperti tampak pada gambar dibawah ini:

![Gambar](img/gambar-05.02.png)

Gambar 05.02

![Gambar](img/gambar-05.03.png)

Gambar 05.03

## Hapus API

- Pada gambar 05.03 pilih **API** yang akan dihapus pada **API LIST**
- Klik tombol `Delete API`

## Membuat Service Rest

- Pada gambar 05.01 klik tombol `New Service`
- Maka akan muncul popup seperti gambar dibawah ini

![Gambar](img/gambar-05.04.png)

Gambar 05.04

- Masukkan nama service misalkan `Pasien`
- Kemudian klik tombol `Create Service`
- Maka akan muncul gambar dibawah ini

![Gambar](img/gambar-05.05.png)

Gambar 05.05

### Konfigurasi Service Rest

#### Ubah Route matches

- Ubah isi `Route matches` `/pasien[/:pasien_id]` menjadi `/pendaftaran/pasien[/:pasien_id]`
- Klik tombol `Save`
- Kemudian tes akses <http://192.168.137.10/api/pendaftaran/pasien>


#### Collection Query String Whitelist

Kegunaan dari `Collection Query String Whitelist` adalah untuk melakukan filter pencarian berdasarkan parameter query yang telah didaftarkan sehingga jika melakukan penambahan parameter query yang belum terdaftar di witelist ini maka akan diabaikan.

Contoh `Query String Whitelist` yang akan ditambahkan:

- `NAMA` untuk pencarian berdasarkan nama pasien
- `start` untuk memulai pencarian dari nomor urut
- `limit` untuk menampilkan batas baris data
- Setelah ditambahkan kemudian klik tombol `Save` untuk menyimpan perubahan

![Gambar](img/gambar-05.06.png)

Gambar 05.06

#### Ubah Content Negotiation

- Klik tab `Content Negotiation`
- Pada `Content Negotiation Selector` ubah dari `HalJson` menjadi `Json`
- Seperti gambar dibawah ini

![Gambar](img/gambar-05.07.png)

Gambar 05.07

### Koneksi Database SQLite

#### Install SQLite

```bash
# Cek apakah sqlite sudah terinstall atau belum
rpm -qa sqlite

# Jika belum diinstall eksekusi perintah dibawah ini
yum install sqlite -y
```

#### Membuat Database

```bash
# Aktif pada direktori latihan
cd /var/www/html/latihan

# Buat direktori db
mkdir -p db

# Masuk ke direktori db
cd db

# Buat database pendaftaran
sqlite3 pendaftaran.db

# Menampilkan databases pada prompt sqlite>
.databases

# Keluar dari command sqlite
.quit
```

#### Membuat Table

```sql
CREATE TABLE pendaftaran.pasien (
    NORM INTEGER PRIMARY KEY AUTOINCREMENT, 
    NAMA VARCHAR(150) NOT NULL,
    JENIS_KELAMIN TINYINT NOT NULL,
    TANGGAL_LAHIR DATE NOT NULL,
    TEMPAT_LAHIR VARCHAR(100) NOT NULL,
    ALAMAT VARCHAR(150) NULL
);
```

```bash
# Jalankan sqlite3
sqlite3

# Attach database
ATTACH DATABASE 'pendaftaran.db' AS 'pendaftaran';

# Copy create table diatas paste ke command sqlite kemudian tekan Enter
```

Seperti gambar dibawah ini

![Gambar](img/gambar-05.08.png)

Gambar 05.08

#### Memasukkan Data

```sql
INSERT INTO pendaftaran.pasien (NORM, NAMA, JENIS_KELAMIN, TANGGAL_LAHIR, TEMPAT_LAHIR, ALAMAT)
VALUES (1, 'Wahidin', 1, '1980-01-01', 'Makassar', 'Jl. Perintis Kemerdekaan IV'), 
(2, 'Fatmawati', 2, '1985-01-01', 'Jakarta', 'Jl. RS. Fatmawati Raya');
```

#### Koneksi DB ke API Tools

- Pada gambar 05.01 klik menu `Database`

![Gambar](img/gambar-05.09.png)

Gambar 05.09

- Klik tombol `New Adapter`

![Gambar](img/gambar-05.10.png)

Gambar 05.10

- Masukkan **Adapter Name:** misalkan `PendaftaranAdapter`
- Pilih **Driver Type:** `PDO_Sqlite`
- **Database:** `pendaftaran`
- **DSN:** `sqlite:/var/www/html/latihan/db/pendaftaran.db`
- Kemudian klik `Save`

### Hubungkan Service Rest dan Database

#### Modifikasi file Entity di API Pendaftaran Service Pasien

Edit file pada folder `latihan/webservice/module/Pendaftaran/src/V1/Rest/Pasien`

```php
<?php
namespace Pendaftaran\V1\Rest\Pasien;

class PasienEntity
{
    public $NORM;
    public $NAMA;
    public $JENIS_KELAMIN;
    public $TANGGAL_LAHIR;
    public $TEMPAT_LAHIR;
    public $ALAMAT;

    public function exchangeArray(array $data) {
        $this->NORM = !empty($data['NORM']) ? $data['NORM'] : null;
        $this->NAMA = !empty($data['NAMA']) ? $data['NAMA'] : null;
        $this->JENIS_KELAMIN = !empty($data['JENIS_KELAMIN']) ? $data['JENIS_KELAMIN'] : null;
        $this->TANGGAL_LAHIR = !empty($data['TANGGAL_LAHIR']) ? $data['TANGGAL_LAHIR'] : null;
        $this->TEMPAT_LAHIR = !empty($data['TEMPAT_LAHIR']) ? $data['TEMPAT_LAHIR'] : null;
        $this->ALAMAT = !empty($data['ALAMAT']) ? $data['ALAMAT'] : null;
    }

    public function getArrayCopy() {
        $data = get_object_vars($this);
        foreach($data as $key => $val) {
            if(empty($val)) unset($data[$key]);
        }
        return $data;
    }
}
```

#### Buat file Service di API Pendaftaran Service Pasien

Buat file `Service.php` pada folder `latihan/webservice/module/Pendaftaran/src/V1/Rest/Pasien`

```bash
cd /var/www/html/latihan/webservice/module/Pendaftaran/src/V1/Rest/Pasien

echo '' > Service.php
```

Edit file `Service.php`

```php
<?php
namespace Pendaftaran\V1\Rest\Pasien;

use Laminas\ApiTools\ApiProblem\ApiProblem;
use Laminas\Db\Adapter\AdapterInterface;
use Laminas\Db\TableGateway\TableGateway;
use Laminas\Db\Sql\Select;

class Service
{
    private $adapter;
    private $table;

    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
        $this->table = new TableGateway("pasien", $adapter);
    }

    public function fetchAll($params = [])
    {
        $params = is_array($params) ? $params : (array) $params;
        return $this->table->select(function(Select $select) use ($params) {
            $start = 0;
            $limit = 25;
            if(isset($params["start"])) {
                $limit = (int) $params["start"];
                unset($params["start"]);
            }
            if(isset($params["limit"])) {
                $limit = (int) $params["limit"];
                unset($params["limit"]);
            }
            $select->offset($start)->limit($limit);
            $select->where($params);
        })->toArray();
    }

    public function get($id)
    {
        $id = (int) $id;
        $rowset = $this->table->select(['NORM' => $id]);
        $row = $rowset->current();
        if (! $row) {
            return new ApiProblem(404, sprintf(
                'Pasien dengan No.RM %d tidak ditemukan',
                $id
            ));
        }

        return $row;
    }

    public function save(PasienEntity $pasien)
    {
        $id = (int) $pasien->NORM;
        $data = $pasien->getArrayCopy();
        $_data = [];

        if ($id === 0) {
            $this->table->insert($data);
            $data["NORM"] = $this->table->getLastInsertValue();
            
            return $data;
        }

        try {
            $_data = (array) $this->get($id);
        } catch (RuntimeException $e) {
            return new ApiProblem(404, sprintf(
                'Pasien dengan No.RM %d; tidak ditemukan',
                $id
            ));
        }

        $this->table->update($data, ['NORM' => $id]);

        return array_merge($_data, $data);
    }

    public function delete($id)
    {
        return $this->table->delete(['NORM' => (int) $id]);
    }
}
```

#### Modifikasi file PasienResourceFactory di API Pendaftaran Service Pasien

Masukkan parameter `$services` ke dalam `PasienResource`

```php
<?php
namespace Pendaftaran\V1\Rest\Pasien;

class PasienResourceFactory
{
    public function __invoke($services)
    {
        return new PasienResource($services);
    }
}
```

#### Modifikasi file PasienResource di API Pendaftaran Service Pasien

- Tambahkan variable dan constructor

```php
    private $service;

    public function __construct($serviceManager) {
        $this->service = new Service($serviceManager->get("PendaftaranAdapter"));
    }
```

- Pada fungsi fetch untuk nilai returnnya di ambil dari service untuk menampilkan data pasien berdasarkan No. RM. Fungsi fetch adalah hasil dari mapping fungsi `GET` pada `HTTP Request`

```php
    public function fetch($id)
    {
        return $this->service->get($id);
    }
```

![Gambar](img/gambar-05.11.png)

Gambar 05.11

- Pada fungsi fetchAll untuk nilai returnnya di ambil dari service untuk menampilkan data pasien berdasarkan parameter filter tertentu yang telah di daftarkan pada [Collection Query String Whitelist](#collection-query-string-whitelist). Fungsi fetchAll adalah hasil dari mapping fungsi `GET` pada `HTTP Request`

```php
    public function fetchAll($params = [])
    {
        if(!empty($params["NAMA"])) {
            $params[] = new \Laminas\Db\Sql\Predicate\Like("NAMA", $params["NAMA"]."%");
            unset($params["NAMA"]);
        }
        return $this->service->fetchAll($params);
    }
```

![Gambar](img/gambar-05.12.png)

Gambar 05.12 - Menampilkan data tanpa parameter dengan default batas baris sebanyak 25 baris

![Gambar](img/gambar-05.13.png)

Gambar 05.13 - Menampilkan data dengan parameter pencarian berdasarkan NAMA

- Pada fungsi create digunakan untuk membuat/menyimpan data pasien baru. Fungsi create adalah hasil dari mapping fungsi `POST` pada `HTTP Request`

```php
    public function create($data)
    {
        $entity = new PasienEntity();
        $entity->exchangeArray((array) $data);
        return $this->service->save($entity);
    }
```

![Gambar](img/gambar-05.14.png)

Gambar 05.14 - Mendaftarkan pasien an. `Nurbaya`

- Pada fungsi update digunakan untuk merubah data pasien. Fungsi update adalah hasil dari mapping fungsi `PUT` pada `HTTP Request`

```php
    public function update($id, $data)
    {
        $data->NORM = $id;
        $entity = new PasienEntity();
        $entity->exchangeArray((array) $data);
        return $this->service->save($entity);
    }
```

![Gambar](img/gambar-05.15.png)

Gambar 05.15 - Merubah nama pasien dari `Nurbaya` menjadi `Siti Nurbaya`

- Pada fungsi delete digunakan untuk hapus data pasien. Fungsi hapus adalah hasil dari mapping fungsi `DELETE` pada `HTTP Request`

```php
    public function delete($id)
    {
        $result = $this->service->delete($id);
        if($result) return new ApiProblem(200, "Pasien dengan No. RM ".$id." berhasil di hapus");
        else return false;
    }
```

![Gambar](img/gambar-05.16.png)

Gambar 05.16 - Menghapus pasien dengan No. RM 2

## STUDI KASUS

- Buatlah table `pendaftaran` pada database pendaftaran diatas. Dengan field sebagai berikut:

```plantuml
@startuml pendaftaran
class pendaftaran {
    NOMOR: INTEGER NOT NULL AUTOINCREMENT
    NORM: INTEGER NOT NULL
    TANGGAL: DATETIME NOT NULL
    STATUS: TINYINT NOT NULL DEFAULT 1
}
@enduml
```

> Catatan: STATUS: 1 = Aktif, 2 = Non Aktif

- Buatlah service `Pendaftaran` pada API `Pendaftaran`
- Lakukan hal yang sama seperti pembuatan service `Pasien`
