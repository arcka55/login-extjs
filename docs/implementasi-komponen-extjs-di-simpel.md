# IMPLEMENTASI KOMPONEN ExtJS DI SIMpel

## PENJELASAN BASE PACKAGE COM & DATA

`Base package` adalah dasar dalam pengembangan / pembuatan module di dalam SIMRSGOS2/SIMpel. Semua Form, Grid, Combox, Model, Store dll harus di inheritance dari class component yang berada pada package `com` & `data`.
`com` untuk menangani `View` sedangkan `data` untuk menangani `Model`.

Adapun fasilitas yang disediakan package `com`, terutama `form` adalah `cetak`, `privillage user access`, `sysdate` dll.

Adapun fasilitas yang disediakan package `data`, terutama `model` adalah access `API`

```plantuml
@startuml
node simpel as "Main App (SIMpel)"
package com as "com"
package data as "data"
package aplikasi as "aplikasi"
package master as "master"
simpel --* com
simpel --* data
simpel --* aplikasi
simpel --o master
aplikasi --^ com
aplikasi --^ data
master --^ com
master --^ data
@enduml
```

`SIMpel` adalah `main aplikasi` yang menggunakan package `com`, `data`, `aplikasi` dan `master`, dimana `main aplikasi` tergantung pada package `com`, `data` dan `aplikasi` yang harus dimasukkan ke dalam `main aplikasi` sedangkan package `master` dapat di masukkan atau tidak di masukkan (optional). Package `aplikasi`, `master` semua komponen yang berada pada package tersebut di turunkan dari package `com` dan `data`.

Sebelum melakukan pengembangan di simpel / simrsgosv2 pertama - tama lakukan `cloning project simrsgosv2` di `git kemkes` atau di `git internal rs` yang telah di install.

Project simrsgosv2 di git kemkes

```bash
git clone http://git.kemkes.go.id/simrs/simrsgosv2.git
```

Kemudian baca `README.md` yg ada di project tsb

## PACKAGE

## MODEL & STORE

## FORM

## GRID

## WORKSPACE

`Workspace` adalah wadah atau tempat untuk menampung komponen `grid` dan `form`.

## DEPLOY APPLICATION TANPA GIT CI/CD
