# IMPLEMENTASI LAMINAS API TOOLS DI SIMpel

## PENJELASAN MODUL BASE DBService

`Bas Modul` adalah dasar dalam pengembangan / pembuatan module di dalam SIMRSGOS2/SIMpel khususnya pada pembuatan `API`. Semua module yang dibuat akan di inheritance ke module base `DBService`.

```plantuml
@startuml
node api as "API"
package DBService as "DBService"
package Aplikasi as "Aplikasi"
package Master as "Master"
api --o DBService
api --o Aplikasi
api --o Master
Aplikasi --^ DBService
Master --^ DBService
@enduml
```

## PENJELASAN MODUL DBService

`DBService` adalah core / base dari semua modul yang dibuat pada pengembangan `API` di simpel / simrsgosv2

### Class DatabaseService

Adalah class untuk mendapatkan adapter

**Contoh Penggunaan:**

Misalkan di database api tools sudah ada adapter `SIMpelAdapter` dan akan mengakses database `master` table `pasien`, maka untuk mendapatkan adapter tersebut:

```php
$database = DatabaseService::get("SIMpel"); // return object database hasil dari instance class Database
$table = $database->get(new TableIdentifier("pasien", "master"));

// atau

$table = DatabaseService::get("SIMpel")->get(new TableIdentifier("pasien", "master"));
```

### Class Database

Adalah sebagai penampung adapter dan digunakan untuk mengambil table. Seperti contoh penggunaan di atas.

### Class generator/Generator

Adalah sebagai tempat untuk mendeklarasikan semua yang berhubungan dengan generate nomor

**Contoh Penggunaan:**

```php
$nopen = Generator::generateNoPendaftaran();
```

### Class SystemArrayObject

Adalah digunakan untuk class `entity` dimana class `entity` akan melakukan `turunan` dari class `SystemArrayObject` dan mendaftaran `fields` dari `table` yang ada di `database`.

### Class Service

Adalah sebagai media untuk melakukan penyimpanan data, penarikan data dan semua service yang dibuat pada modul - modul harus merupakan turunan dari class service ini.

#### Fungsi queryCallback pada class Service

Adalah sebagai tempat untuk melakukan fungsi `where`, `join` sebelum pengambilan data.

#### Fungsi onBeforeSaveCallback pada class Service

Digunakan untuk melakukan kondisi atau perubahan tertentu sebelum di simpan. Misalkan ingin mengenerate nomor pendaftaran.

#### Fungsi onAfterSaveCallback pada class Service

Digunakan untuk melakukan sesuatu sesudah data di simpan. Misalkan pada saat data pendaftaran di simpan maka akan melakukan penyimpanan ke service lainnya (penjamin).

### Fungsi load pada class Service

Digunakan untuk mengambil data dari table dan melakukan penggabungan data atau biasa di sebut sebagai data referensi.

### Class Resource

Class Custom untuk menghandle service `Rest` dimana `service` yang dibuat di `API` melalui API Tools maka file `Resource` harus menjadi turunan dari class `Resource` ini. Dimana class ini menyederhanakan proses standar `Rest` sebelumnya.

### Class RPCResource

Sama seperti class `Resource`, dimana class ini menyederhanakan proses standar `RPC` sebelumnya.

## CREATE DB

- Buka `HeidiSQL`
- Set Koneksi ke Database MySQL
  - Host: `192.168.137.7`
  - Username: `admin`
  - Password: `S!MRSGos2`
- Buat database `pendaftaranx`
- Buat table `pasien`

```sql
CREATE TABLE pendaftaranx.pasien (
    NORM INTEGER PRIMARY KEY AUTOINCREMENT, 
    NAMA VARCHAR(150) NOT NULL,
    JENIS_KELAMIN TINYINT NOT NULL,
    TANGGAL_LAHIR DATE NOT NULL,
    TEMPAT_LAHIR VARCHAR(100) NOT NULL,
    ALAMAT VARCHAR(150) NULL
);
```

## CREATE API DAN SERVICE

Sebagai contoh kita akan membuat `API` dengan nama `PendaftaranX` dan `web service` dengan nama pasien.

Cara pembuatan `API` dan `Web Service` sama dengan pelatihan [laminas api tools](laminas-api-tools.md)

Setelah `API` dan `Web Service` dibuat di konfigurasi, langkah selanjutnya membuat file `Service`, Edit file `Entity`, `Resource` dan `ResourceFactory`

## ENTITY

- Edit file `PasienEntity.php`
- Tambahkan `use DBService\SystemArrayObject;` sebelum `class`
- Class `PasienEntity` turunan dari class `SystemArrayObject`
- Tambahkan kode dibawah ini pada body class

```php
protected fields = [
    "NORM" => 1,
    "NAMA" => [
        "REQUIRED" => true
    ],
    "TEMPAT_LAHIR" => [
        "REQUIRED" => true
    ],
    "TANGGAL_LAHIR" => [
        "REQUIRED" => true
    ],
    "JENIS_KELAMIN" => [
        "REQUIRED" => true
    ],
    "ALAMAT" => 1
];
```

## SERVICE

- Buat File `Service.php` pada folder `webservice/module/PendaftaranX/src/V1/Rest/Pasien`
- Tambahkan kode dibawah ini:

```php
<?php
namespace PendaftaranX\V1\Rest\Pasien;

use DBService\DatabaseService;
use Laminas\Db\Sql\TableIdentifier;
use DBService\Service as DBService;

class Service extends DBService
{
    protected $references = [];
    
    public function __construct($includeReferences = true, $references = []) {
        $this->config["entityName"] = "PendaftaranX\\V1\\Rest\\Pasien\\PasienEntity";
        $this->config["entityId"] = "NORM";
        $this->table = DatabaseService::get('SIMpel')->get(new TableIdentifier("pasien", "pendaftaranx"));
        $this->entity = new PasienEntity();
    }
}
```

## RESOURCE

- Edit file `PasienResource.php`
- Ubah `use Laminas\ApiTools\Rest\AbstractResourceListener;` menjadi `use DBService\Resource;`
- Class `PasienResource` turunan dari class `Resource` module `DBService`
- Deklarasikan kembali attribute/field $title `protected $title = "Pasien";`
- Deklarasikan constructor seperti di bawah ini

```php
    public function __construct() {
        parent::__construct();
        $this->service = new Service();      
    }
```

- Pada fungsi `create` ubah kode didalamnya menjadi

```php
$result = parent::create($data);
return $result;
```

- Pada fungsi `fetch` ubah kode didalamnya menjadi

```php
$result = parent::fetch($id);
return $result;
```

- Pada fungsi `fetchAll` ubah kode didalamnya menjadi

```php
parent::fetchAll($params);
$order = ["ID DESC"];
$data = null;        
if (isset($params->sort)) {
    $orders = json_decode($params->sort);
    if(is_array($orders)) {
    } else {
        $orders->direction = strtoupper($orders->direction);
        $order = [$orders->property." ".($orders->direction == "ASC" || $orders->direction == "DESC" ? $orders->direction : "")];
    }
    unset($params->sort);
}
$params = is_array($params) ? $params : (array) $params;
if (isset($params["NOT"])) {
    $nots = (array) json_decode($params["NOT"]);
    foreach($nots as $key => $val) {                
        $params[] = new \Laminas\Db\Sql\Predicate\Expression("(NOT pasien.".$key." = ".$val.")");                
    }
    unset($params["NOT"]);
}

$total = $this->service->getRowCount($params);
if($total > 0) $data = $this->service->load($params, ['*'], $order);

return [
    "status" => $total > 0 ? 200 : 404,
    "success" => $total > 0 ? true : false,
    "total" => $total,
    "data" => $data,
    "detail" => $this->title.($total > 0 ? " ditemukan" : " tidak ditemukan")
];
```

- Pada fungsi `update` ubah kode didalamnya menjadi

```php
$result = parent::update($id, $data);
return $result;
```

## RESOURCE FACTORY

- Edit file `PasienResourceFactory.php`
- Ubah kode pada fungsi `__invoke` menjadi

```php
$obj = new PasienResource();
$obj->setServiceManager($services);
return $obj;
```

## JOIN DENGAN API GENERAL MODULE REFERENSI

Disini kita akan mengambil referensi `jenis kelamin` dengan memanggil `service referensi`.

- Edit file `Service.php`
- Tambahkan kode ini sebelum deklarasi `class` `General\V1\Rest\Referensi\ReferensiService;`
- Tambahkan field `$referensi` dengan modifier `private`
- Tambahkan field `$references` dengan modifier `protected` set variable tersebut menjadi `array` dimana didalamnya ada field `"Referensi" => true`
- Tambahkan kode dibawah ini setelah deklarasi pembuatan objek `entity` pada `__construct`

```php
$this->setReferences($references);
$this->includeReferences = $includeReferences;
if($includeReferences) {
    $this->referensi = new Referensi();
}
```

- Lakukkan `override` fungsi `load` seperti kode dibawah ini

```php
public function load($params = [], $columns = ['*'], $orders = []) {
    $data = parent::load($params, $columns, $orders);
    
    if($this->includeReferences) {
        foreach($data as &$entity) {
            if($this->references['Referensi']) {
                $referensi = $this->referensi->load(array('JENIS' => 2,'ID' => $entity['JENIS_KELAMIN']));
                if(count($referensi) > 0) $entity['REFERENSI']['JENIS_KELAMIN'] = $referensi[0];
            }
        }
    }
    
    return $data;
}
```

## PENGGUNAAN QueryCallback PADA Service

- Edit file `Service.php`
- Tambahkan kode `use Laminas\Db\Sql\Select;` sebelum `class`
- Override fungsi `queryCallback` seperti kode dibawah ini

```php
protected function queryCallback(Select &$select, &$params, $columns, $orders) {
    $select->join(['ref'=>new TableIdentifier('referensi', 'master')], 'ref.ID = pasien.JENIS_KELAMIN', []);
    $select->where("ref.JENIS = 2");
}
```
